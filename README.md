# Placeholder images

Inspired by Trivago's [SQIP](https://github.com/technopagan/sqip) which is based on [Primitive](https://github.com/fogleman/primitive) implemented in Go.

The goal is to create textual representations of images by generating random objects and hill-climbing towards the input image.

The process starts with a canvas filled with the average color of the input image. Then the difference between of a certain area and the original image is compared and a random object placed on inside of it. If this mutation brought the image closer to the original image the object is kept in place and written to an SVG.

## Setup
Make sure you have installed Python 3 and [pipenv](https://docs.pipenv.org/) by running
```bash
python3 --version
pipenv
```

If one of the above commands fails install missing packages by running
```bash
brew install python
brew install pipenv
```

After installing the needed packages initialize your local environment with
```bash
pipenv --three
pipenv install
```

## Operation
Feel free to adjust any of the constants on the top of `main.py` to change or improve results and then run the script by executing
```bash
pipenv run python main.py
```

Keep an eye on the `results/*-test.jpg` images to see the process in action.
