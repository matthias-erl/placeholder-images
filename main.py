import collections
import math
import os
import random

import svgwrite
from haishoku.haishoku import Haishoku
from PIL import Image, ImageChops, ImageDraw, ImageFilter

# Maximum iteration count that is performed to find scoring objects
MAX_ITERATIONS = 100
# Maximum count of objects that are saved to the SVG
MAX_OBJECTS = 6
# Size the input image is cropped to and the candidate image is evaluated in
CROP_SIZE = 256, 256
# Minimum radius the generated ellipses should have
ELLIPSE_MIN_LENGTH = 40
# Score threshold that has to be met for mutations
SCORE_THRESHOLD = 8


def rgb_to_hex(tuple):
    """
    Convert a rgb color tuple like (255, 255, 255) to the corresponding HEX value
    """
    return '#{0:02x}{1:02x}{2:02x}'.format(*tuple)


def calculate_difference(im1, im2):
    """
    Calculate the Root-mean-squared error between two input images
    """
    diff = ImageChops.difference(im1, im2)
    h = diff.histogram()
    sq = (value * ((idx % 256)**2) for idx, value in enumerate(h))
    sum_of_squares = sum(sq)
    rms = math.sqrt(sum_of_squares / float(im1.size[0] * im1.size[1]))
    return rms


"""
Named ellipse tuple used to pass generated ellipses between methods
"""
Ellipse = collections.namedtuple('Ellipse', 'x1 y1 x2 y2 length height color')


class PlaceholderImage(object):
    def __init__(self, path):
        # Store a resized version of the image inside the object
        self.image = Image.open(path)
        self.image.thumbnail(CROP_SIZE)
        # Store width and height for quick access
        self.width = self.image.width
        self.height = self.image.height
        # Parse name for later use
        self.name = os.path.splitext(os.path.basename(path))[0]
        # Get color plaette
        haishoku = Haishoku.loadHaishoku(path)
        self.dominant_color = haishoku.dominant
        # Remove dominant color from color palette
        self.color_palette = haishoku.palette[1:]
        # Create a candidate image to perform mutations on
        if self.image.mode == 'RGB':
            self.candidate = Image.new('RGB', self.image.size, self.dominant_color)
        elif self.image.mode == 'RGBA':
            self.candidate = Image.new('RGBA', self.image.size)
        self.drawer = ImageDraw.Draw(self.candidate)
        # Bootstrap SVG object that resembles placeholder image and expose the
        # group to be able to apply a blur
        self.svg, self.objects = self.get_svg()
        # And then already generate the placeholder SVG
        self.generate()

    def get_svg(self):
        svg = svgwrite.Drawing(
            '{}.svg'.format(self.name),
            profile='full',
            size=('{}px'.format(self.width), '{}px'.format(self.height)))

        # Fill the SVG with the dominant color as background but only if
        # the image does not contain alpha
        if self.image.mode == 'RGB':
            svg.add(
                svg.rect(
                    insert=(0, 0),
                    size=('100%', '100%'),
                    fill=rgb_to_hex(self.dominant_color)))

        # Add the blur filter
        blur = svg.defs.add(svg.filter())
        blur.feGaussianBlur(in_='SourceGraphic', stdDeviation=40)

        # Create to group to hold the elements in
        objects = svg.add(svg.g(filter=blur.get_funciri()))

        return svg, objects

    def get_random_ellipse(self):
        x1 = random.randint(0, self.image.width)
        y1 = random.randint(0, self.image.height)
        x2 = random.randint(x1, self.image.width)
        y2 = random.randint(y1, self.image.height)

        # Make sure that the generated ellipse is not too narrow on any axis
        if x2 - x1 < ELLIPSE_MIN_LENGTH:
            x2 += ELLIPSE_MIN_LENGTH
        if y2 - y1 < ELLIPSE_MIN_LENGTH:
            y2 += ELLIPSE_MIN_LENGTH

        length = x2 - x1
        height = y2 - y1

        color = random.choice(self.color_palette)[1]

        return Ellipse(
            x1=x1,
            y1=y1,
            x2=x2,
            y2=y2,
            length=length,
            height=height,
            color=color)

    def score_ellipse(self, ellipse):
        original = self.image.crop((ellipse.x1, ellipse.y1, ellipse.x2,
                                    ellipse.y2))
        candidate = self.candidate.crop((ellipse.x1, ellipse.y1, ellipse.x2,
                                         ellipse.y2))
        return calculate_difference(original, candidate)

    def save_ellipse(self, ellipse):
        # Convert color to HEX prior adding it to the SVG to save bytes
        color = rgb_to_hex(ellipse.color)
        center = (ellipse.x1 + (ellipse.length // 2),
                  ellipse.y1 + (ellipse.height // 2))
        radius = (ellipse.length // 2, ellipse.height // 2)
        self.objects.add(self.svg.ellipse(center=center, r=radius, fill=color))

    def generate(self):
        print('Finding placeholder for:', self.name)
        # Count the iterations that have been done trying to find scoring elements
        # and keep track of how many objects have been already added
        iterations, object_count = 0, 0
        while object_count < MAX_OBJECTS and iterations < MAX_ITERATIONS:
            # Keep the current state of the candidate image to revert to after
            # unsuccessful mutations
            current_candidate = self.candidate.copy()
            ellipse = self.get_random_ellipse()
            # First calculate the score for the current state before the mutation
            print('{}/{}: {}'.format(iterations, MAX_ITERATIONS, ellipse))
            current_score = self.score_ellipse(ellipse)
            print('-- Current score:', current_score)
            # Paint the new ellipse on the candidate image to score it
            self.drawer.ellipse([(ellipse.x1, ellipse.y1),
                                 (ellipse.x2, ellipse.y2)], ellipse.color)
            score = self.score_ellipse(ellipse)
            print('-- Mutated score:', score)

            self.candidate.save('results/' + self.name + '-test.png')

            if score < current_score and current_score - score > SCORE_THRESHOLD:
                # If the score decreased the mutation can be kept
                print(
                    '--> Decreased score by {}'.format(current_score - score))
                self.save_ellipse(ellipse)
                object_count += 1
            else:
                # When the score did not decrease the mutation was unsuccessful
                # and we need to revert to the previously saved state
                self.candidate = current_candidate
                self.drawer = ImageDraw.Draw(self.candidate)
            iterations += 1
        self.save()

    def save(self):
        self.svg.saveas('results/' + self.name + '.svg')
        image = self.candidate.filter(ImageFilter.GaussianBlur(50))
        if image.mode == 'RGBA':
            image.save('results/' + self.name + '-blurred.png')
        elif image.mode == 'RGB':
            image.save('results/' + self.name + '-blurred.jpg')

# Make sure results directory exists
if not os.path.exists('results'):
    os.makedirs('results')

# PlaceholderImage('images/1.jpg').save()
# PlaceholderImage('images/2.jpg').save()
# PlaceholderImage('images/3.jpg').save()
# PlaceholderImage('images/4.jpg').save()
# PlaceholderImage('images/5.jpg').save()
PlaceholderImage('images/6.png').save()
